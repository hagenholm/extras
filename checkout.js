
console.log(1);

var pSettings = {
    progressBg: "#23468c",
    progressBg2: "#d9e3f7",
    highDemandText: "Tu pedido esta casi realizado, solo falta realizar el pago, No te preocupes, hemos reservado tu pedido.",
    whyUsImg1: "https://cdn.shopify.com/s/files/1/1319/2435/t/4/assets/money-back.png",
    whyUsTitle1: "Garantía de satisfacción de 30 días con devolución de dinero",
    whyUsText1: "Si no está satisfecho con sus productos, emitiremos un reembolso completo, sin preguntas.",
    whyUsImg2: "https://cdn.shopify.com/s/files/1/1319/2435/t/4/assets/mail-truck.png",
    whyUsTitle2: "Más de 7.245 pedidos enviados con éxito",
    whyUsText2: "Hicimos tantos clientes felices como muchos pedidos enviados. Simplemente tienes que unirte a nuestra gran familia."
};

function crC(e, t, s) {
    if (s) {
        var i = new Date;
        i.setTime(i.getTime() + 60 * s * 1e3);
        var n = "; expires=" + i.toUTCString()
    } else n = "";
    document.cookie = e + "=" + t + n + "; path=/"
}

function rdC(e) {
    for (var t = e + "=", s = document.cookie.split(";"), i = 0; i < s.length; i++) {
        for (var n = s[i];
            " " == n.charAt(0);) n = n.substring(1, n.length);
        if (0 == n.indexOf(t)) return n.substring(t.length, n.length)
    }
    return null
}

function eSC(e) {
    crC(e, "", -1)
}

function stTM(e, t, s) {
    var i, n, a;

    function o() {
        i = t - ((Date.now() - e) / 1e3 | 0), a = i % 60 | 0, n = (n = i / 60 | 0) < 10 ? "0" + n : n, a = a < 10 ? "0" + a : a, s.textContent = n + ":" + a, i <= 0 && (clearInterval(d), document.getElementById("ct836").innerHTML = "La reserva de pedido finalizó.", e = Date.now() + 1e3)
    }
    o();
    var d = setInterval(o, 1e3)
}
var wnd = window.location.href,
    chsg = '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24" fill="#fff"><path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/></svg>';
wnd.indexOf("checkout") > -1 && (window.onload = function() {
    if (wnd.indexOf("checkout") > -1 && (dsXt = document.getElementById("checkout_reduction_code"), document.body.insertAdjacentHTML("afterbegin", '<div class="content prH7"><div class="wrap"><div class="ar64"><div id="sp1" class="s8 s8c"><span id="spn1">1. Información</span></div><div id="sp2" class="s8"><span id="spn2">2. Envío</span></div><div id="sp3" class="s8"><span id="spn3">3. Pago</span></div></div></div></div>'), -1 === wnd.indexOf("thank_you"))) {
        document.getElementsByClassName("section--payment-method")[0].insertAdjacentHTML("afterbegin", '<div style="display:block;background: #d1ecf1;padding:10px 20px;border: 1px solid #bee5eb;font-size: 17px;color: #0c5460;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;margin:10px 0px;text-align: center;">Paga también con tu CUENTA RUT.</div>'), document.getElementsByClassName("main__content")[0].insertAdjacentHTML("afterbegin", '<div><div id="ct836" style="display:block;background:#fff5d2;padding:10px 20px;border:1px solid #fac444;font-size:14px;color:#2c2c2c;font-weight:bold;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px; margin:5px 0px 20px 0px">Tu pedido está siendo reservado por <span id="time"></span> minutos!</div><div class="countdownholder">');
document.getElementsByClassName("logo").remove(0);

        var e = 600,
            t = Date.now(),
            s = rdC("pRtC");
        s ? t < s ? e = (s - t) / 1e3 : (eSC("pRtC"), crC("pRtC", Date.now() + 1e3 * e, e + 1)) : crC("pRtC", Date.now() + 1e3 * e, e + 1), display = document.querySelector("#time"), stTM(t, e, display), document.getElementsByClassName("main__content")[0].insertAdjacentHTML("afterbegin", '<div style="width:100%;display:table"><div style="display:table-cell;vertical-align:middle"><img src="https://cdn.shopify.com/s/files/1/1319/2435/t/4/assets/flame_24.png?10413921915994220473"></div><div style="font-weight:bold;padding-left:5px">' + pSettings.highDemandText + "</div></div>"), "" != pSettings.discountText && dsXt && dsXt.setAttribute("placeholder", pSettings.discountText), document.getElementsByClassName("step__footer")[0].insertAdjacentHTML("afterend", '<div style="width:100%;display:block;padding-top:10px"><span style="font-size:11px;line-height:12px;font-style=italic;float:right;width:100%;text-align:right">Garantizado y Seguro!</span><img src="https://cdn.shopify.com/s/files/1/0072/2554/0672/files/paybadges.jpg" style="float: right;margin-top: 25px;"></div>'), document.getElementsByClassName("order-summary__sections")[0].insertAdjacentHTML("beforeend", '<div style="position:relative;padding:10px 0px"><div class="wyustit" style="position:relative;z-index:1;text-align:center"><span style="background:#fafafa;padding:0 15px">¿Por qué elegirnos?</span></div><div style="display:table;vertical-align:middle;width:100%;border-spacing:0px 20px"><div class="wyuscs"><div class="wyuscs1"><img src="' + pSettings.whyUsImg1 + '"></div><div class="wyuscs2"><span>' + pSettings.whyUsTitle1 + "</span><p>" + pSettings.whyUsText1 + '</p></div></div><div class="wyuscs"><div class="wyuscs1"><img src="' + pSettings.whyUsImg2 + '"></div><div class="wyuscs2"><span>' + pSettings.whyUsTitle2 + "</span><p>" + pSettings.whyUsText2 + "</p></div></div></div><img src='https://cdn.shopify.com/s/files/1/0072/2554/0672/files/Reviews.png' style='margin:0 auto;display:none'>")
    }
    b1j = document.getElementById("sp1"), b2j = document.getElementById("sp2"), b3j = document.getElementById("sp3"), c1j = document.getElementById("spn1"), c2j = document.getElementById("spn2"), c3j = document.getElementById("spn3"), wnd.indexOf("previous_step=contact_information") > -1 && (b1j.className = "s8 s8c", b2j.className = "s8 s8c", c1j.insertAdjacentHTML("afterbegin", chsg)), wnd.indexOf("previous_step=shipping_method") > -1 && (b1j.className = "s8 s8c", b2j.className = "s8 s8c", b3j.className = "s8 s8c", c1j.insertAdjacentHTML("afterbegin", chsg), c2j.insertAdjacentHTML("afterbegin", chsg)), wnd.indexOf("thank_you") > -1 && (b1j.className = "s8 s8c", b2j.className = "s8 s8c", b3j.className = "s8 s8c", c1j.insertAdjacentHTML("afterbegin", chsg), c2j.insertAdjacentHTML("afterbegin", chsg), c3j.insertAdjacentHTML("afterbegin", chsg))
});
var b7 = document.createElement("style");
b7.type = "text/css", b7.innerHTML = ".main ul.breadcrumb{display:none!important}.content.prH7{padding:8px 0}.ar64{width:100%}.ar64 .s8{font-size:14px;text-align:center;color:#fff;cursor:default;margin:0 3px;padding:9px 10px 9px 30px;float:left;position:relative;background-color:" + pSettings.progressBg2 + ';-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;transition:all 2s ease;width:20%}.ar64 .s8:after,.ar64 .s8:before{content:" ";position:absolute;top:0;right:-17px;width:0;height:0;border-top:19px solid transparent;border-bottom:17px solid transparent;border-left:17px solid ' + pSettings.progressBg2 + ";z-index:2;transition:border-color .2s ease}.ar64 .s8:before{right:auto;left:0;border-left:17px solid #fff;z-index:0}.ar64 .s8:first-child:before{border:none}.ar64 .s8:first-child{margin-left:0;border-top-left-radius:4px;border-bottom-left-radius:4px}.ar64 .s8 span{position:relative}.ar64 .s8.s8c{color:#fff;background-color:" + pSettings.progressBg + "}.ar64 .s8.s8c:after{border-left:17px solid " + pSettings.progressBg + '}.ar64 .s8 svg{position:absolute;left:-17px;top:2px}.ar64 .s8:first-child svg{left:-16px}.wyustit:before{border-top:1px solid #dfdfdf;content:"";margin:0 auto;position:absolute;top:50%;left:0;right:0;bottom:0;width:100%;z-index:-1}.wyuscs{display:table-row;padding-bottom:20px}.wyuscs1,.wyuscs2{display:table-cell;vertical-align:middle}.wyuscs1{width:20%;font-size:15px}.wyuscs2{width:80%}.wyuscs2 span{font-size:14px;font-weight:700;color:#666}.wyuscs2 p{font-size:12px;color:#777}@media(min-width:450px){.ar64 .s8{min-width:29%}.ar64 .s8 svg{position:relative!important;top:2px!important;left:-10px!important}}@media(max-width:750px){.ar64 .s8{font-size:11px}.ar64 .s8:first-child{padding-left:20px}}', document.body.appendChild(b7);